<?php

/**
 * @file
 * Interface and Controller for FortyK entities.
 *
 * @see fortyk.module
 */

/**
 * FortyK class.
 */
class FortyK extends Entity {
  /**
   * Defines the entity label if the 'entity_class_label' callback is used.
   *
   * Specify 'entity_class_label' as 'label callback' in hook_entity_info() to
   * let the entity label point to this method. Override this in order to
   * implement a custom default label.
   */
  protected function defaultLabel() {
    return t($this->identifier());
  }

  /**
   * Returns the uri of the entity just as entity_uri().
   *
   * Modules may alter the uri by specifying another 'uri callback' using
   * hook_entity_info_alter().
   *
   * @see entity_uri()
   */
  protected function defaultUri() {
    return array('path' => 'admin/content/fortyk/' . $this->identifier() . '/edit');
  }
}

/**
 * EventLocationController extends EntityAPIController.
 *
 * Our subclass of EntityAPIController lets us add a few
 * customizations to create, update, and delete methods.
 */
class FortyKController extends EntityAPIController {

  /**
   * Create a new entity.
   *
   * @param array $values
   *   An array of values to set, keyed by property name.
   * @return
   *   A new instance of the entity type.
   */
  public function create(array $values = array()) {
    $values += array(
      'tid' => NULL,
    );
    return parent::create($values);
  }

}
