<?php

/**
 * @file
 * Administration pages for FortyK module.
 *
 * @see fortyk.module
 */

/**
 * Administration page.
 */
function fortyk_admin_page() {
  $output = array();
  $output[] = array(
    '#prefix' => '<h2 class="title">',
    '#markup' => t('Cornerstone 40k Invitations'),
    '#suffix' => '</h2>',
  );

  $output[] = array(
    '#prefix' => '<h3 class="title">',
    '#markup' => t('Add invitation'),
    '#suffix' => '</h3>',
  );

  $output[] = array(drupal_get_form('fortyk_invite_form', 'adminform'));

  $output[] = array(
    '#prefix' => '<h3 class="title">',
    '#markup' => t('Current totals'),
    '#suffix' => '</h3>',
  );

  $output[] = array(
    '#markup' => theme('fortyk_tally', fortyk_tally()),
  );

  $output[] = array(
    '#prefix' => '<h3 class="title">',
    '#markup' => t('Unprocessed SMS messages'),
    '#suffix' => '</h3>',
  );

  $output[] = array(drupal_get_form('fortyk_admin_form'));

  return $output;
}

/**
 * Display all submissions.
 */
function fortyk_admin_sms() {
  $result = db_query("SELECT * FROM {fortyk} ORDER BY created DESC")->fetchAll();
  $rows = array();
  foreach ($result as $item) {
    $rows[] = array(
      $item->source,
      $item->sanctuary,
      $item->momentum,
      $item->ignited,
      $item->kotr,
      check_plain($item->message),
      format_date($item->created, 'custom', 'n/j/Y g:ia'),
      '#markup' => l('delete', 'admin/content/fortyk/' . $item->tid . '/delete'),
    );
  }

  $limit = 50;
  $page = pager_default_initialize(count($rows), $limit, 0);
  $offset = $limit * $page;

  return array(
    array(
      '#theme' => 'table',
      '#header' => array(
        t('Source'),
        t('Sanctuary'),
        t('Momentum'),
        t('Ignited'),
        t('KOTR'),
        t('Message'),
        t('Date added'),
        t('Actions'),
      ),
      '#rows' => array_slice($rows, $offset, $limit),
    ),
    array(
      '#theme' => 'pager',
    ),
  );

}

/**
 * Admin form to process unparsed messages.
 */
function fortyk_admin_form($form, &$form_state) {
  $query = db_select('fortyk', 'f')
    ->fields('f')
    ->condition('f.sanctuary', 0)
    ->condition('f.momentum', 0)
    ->condition('f.ignited', 0)
    ->condition('f.kotr', 0)
    ->OrderBy('f.created', 'DESC');
  $result = $query->execute()->fetchAll();

  $form['messages'] = array(
    '#theme' => 'fortyk_admin_table',
    '#tree' => TRUE,
  );

  foreach ($result as $item) {
    $form['messages'][$item->tid] = array(
      'item' => array(
        '#type' => 'value',
        '#value' => $item,
      ),
      'source' => array(
        '#markup' => check_plain($item->source),
      ),
      'sanctuary' => array(
        '#type' => 'textfield',
        '#size' => '4',
        '#maxlength' => 10,
        '#default_value' => trim($item->sanctuary),
        '#element_validate' => array('element_validate_integer'),
      ),
      'momentum' => array(
        '#type' => 'textfield',
        '#size' => '4',
        '#maxlength' => 10,
        '#default_value' => trim($item->momentum),
        '#element_validate' => array('element_validate_integer'),
      ),
      'ignited' => array(
        '#type' => 'textfield',
        '#size' => '4',
        '#maxlength' => 10,
        '#default_value' => trim($item->ignited),
        '#element_validate' => array('element_validate_integer'),
      ),
      'kotr' => array(
        '#type' => 'textfield',
        '#size' => '4',
        '#maxlength' => 10,
        '#default_value' => trim($item->kotr),
        '#element_validate' => array('element_validate_integer'),
      ),
      'message' => array(
        '#markup' => check_plain($item->message),
      ),
      'created' => array(
        '#markup' => format_date($item->created, 'custom', 'n/j/Y g:ia'),
      ),
      'actions' => array(
        '#markup' => l('delete', 'admin/content/fortyk/' . $item->tid . '/delete'),
      ),
    );
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
    '#weight' => 100,
  );

  return $form;
}

/**
 * Submit handler for admin form.
 */
function fortyk_admin_form_submit($form, &$form_state) {
  foreach ($form_state['values']['messages'] as $key => $data) {
    $item = $data['item'];
    // we are only interested in changed records.
    if ($data['sanctuary'] !== $item->sanctuary || $data['momentum'] !== $item->momentum || $data['ignited'] !== $item->ignited || $data['kotr'] !== $item->kotr ) {

      db_update('fortyk')
        ->fields(array(
          'sanctuary' => $data['sanctuary'],
          'momentum' => $data['momentum'],
          'ignited' => $data['ignited'],
          'kotr' => $data['kotr'],
        ))
        ->condition('tid', $item->tid, '=')
        ->execute();
    }
  }
}

/**
 * Item delete confirmation form.
 */
function fortyk_delete_confirm($form, &$form_state, $entity) {
  // drupal_set_title(t('Are you sure you want to delete this entry from %source?', array('%source' => $item->source)));

  $form['#entity'] = $entity;
  // Always provide entity id in the same form key as in the entity edit form.
  $form['tid'] = array(
    '#type' => 'value',
    '#value' => $entity->tid,
  );

  return confirm_form($form,
    t('Are you sure you want to delete this entry from %source?', array('%source' => $entity->source)),
    'admin/content/fortyk', t('This action cannot be undone.'), t('Delete'), t('Cancel')
  );
}

/**
 * Submit handler for delete confirmation.
 */
function fortyk_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm'] == 1 && !empty($form_state['values']['tid'])) {
    $num_deleted = db_delete('fortyk')
      ->condition('tid', $form_state['values']['tid'])
      ->execute();
    $values = array('%tid' => $form_state['values']['tid']);
    if ($num_deleted == 1) {
      drupal_set_message(t('Item %tid deleted.', $values));
    }
    else {
      watchdog('fortyk', 'Something bad happened deleting item %tid', $values, WATCHDOG_ERROR);
    }
  }
  drupal_goto('admin/content/fortyk');
}

/**
 * Message Parser Test form.
 */
function fortyk_parse_test($form, &$form_state) {

  $form[] = array(
    '#prefix' => '<h2 class="title">',
    '#markup' => t('Message parser test'),
    '#suffix' => '</h2>',
  );

  if (!empty($form_state['values']['message'])) {
    $message = $form_state['values']['message'];
    $form[] = array(
      '#title' => t('Test Results for %message', array('%message' => $message)),
      '#type' => 'textarea',
      '#default_value' => print_r(fortyk_message_parser($message), 1),
      '#disabled' => TRUE,
    );
  }
  else {
    $message = NULL;
  }

  $form['message'] = array(
    '#type' => 'textfield',
    '#title' => t('Message'),
    '#default_value' => $message,
    '#description' => t('The message to test.'),
    '#size' => 160,
    '#maxlength' => 255,
    '#required' => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Test'),
    '#weight' => 100,
  );

  return $form;
}

/**
 * Submit handler for Message Parser Test form.
 */
function fortyk_parse_test_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
}
