<?php

/**
 * @file
 * Contains rules integration functionality for the FortyK module.
 *
 * @see fortyk.module
 */

/**
 * Process received SMS message.
 */
function fortyk_rules_action_register_invites($sms_number, $sms_message) {

  if (!empty($sms_number) && !empty($sms_message)) {

    // $variables = array('%number' => $sms_number, '%message' => $sms_message);
    // watchdog('fortyk', 'Message from %number: %message', $variables, WATCHDOG_NOTICE);

    $values = fortyk_message_parser($sms_message);

    $entity = entity_create('fortyk', $values += array(
      'source' => $sms_number,
      'uid' => 0,
      'created' => time(),
    ));
    entity_save('fortyk', $entity);
  }
}
