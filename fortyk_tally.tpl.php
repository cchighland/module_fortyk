<?php

/**
 * @file
 * Theme implementation for fortyk tally.
 *
 * Available variables:
 * - $content: content rendered as a simple table.
 *
 * - $sanctuary: count for Main Sanctuary.
 * - $momentum:  count for Momentum.
 * - $ignited:   count for Ignited.
 * - $kotr:      count for KOTR.
 * - $total:     total count.
 *
 * @see template_preprocess_fortyk_tally()
 */
?>

<?php print render($content); ?>
