<?php

/**
 * @file
 * Contains theme functions for FortyK module.
 *
 * @see fortyk.module
 */

/**
 * Theme preprocess function for theme_fortyk_tally() and fortyk-tally.tpl.php.
 *
 * @see template_preprocess()
 */
function template_preprocess_fortyk_tally(&$variables) {

  $variables['content'] = array(
    '#theme' => 'table',
    '#rows' => array(
      array(
        'data' => array(
          array('data' => t('Main Sanctuary'), 'class' => array('label')),
          array('data' => $variables['sanctuary'], 'class' => array('value')),
        ),
        'class' => array('sanctuary'),
      ),
      array(
        'data' => array(
          array('data' => t('Momentum'), 'class' => array('label')),
          array('data' => $variables['momentum'], 'class' => array('value')),
        ),
        'class' => array('momentum'),
      ),
      array(
        'data' => array(
          array('data' => t('Ignited'), 'class' => array('label')),
          array('data' => $variables['ignited'], 'class' => array('value')),
        ),
        'class' => array('ignited'),
      ),
      array(
        'data' => array(
          array('data' => t('KOTR'), 'class' => array('label')),
            array('data' => $variables['kotr'], 'class' => array('value')),
        ),
        'class' => array('kotr'),
      ),
      array(
        'data' => array(
          array('data' => t('Total'), 'class' => array('label')),
           array('data' => $variables['total'], 'class' => array('value')),
        ),
        'class' => array('total'),
      ),
    ),
    '#attributes' => array('class' => array('fortyk-tally-table')),
  );

}

/**
 * Implemented using the fortyk-tally.tpl.php template.
 */
function theme_fortyk_tally($variables) {
  return $content;
}

/**
 * Theme admin form table.
 */
function theme_fortyk_admin_table($variables) {
  $element = $variables['element'];
  $header = array(
    'source' => t('Source'),
    'sanctuary' => t('Sanctuary'),
    'momentum' => t('Momentum'),
    'ignited' => t('Ignited'),
    'kotr' => t('KOTR'),
    'message' => t('Message'),
    'created' => t('Date added'),
    'actions' => t('Actions'),
  );

  $rows = array();
  foreach (element_children($element) as $key) {
    $row = array();
    $row['data'] = array();
    foreach ($header as $fieldname => $title) {
      $row['data'][] = drupal_render($element[$key][$fieldname]);
    }
    $rows[] = $row;
  }

  return theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'attributes' => array('id' => 'degree-types-table')
  ));
}
